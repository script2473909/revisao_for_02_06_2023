#!/bin/bash

while true; do
	echo "Opção (1) Digite o nome de um arquivo"
	echo "Opção (2) Criar o arquivo"
	echo "Opção (3) Adicionar um número ao final do arquivo"
	echo "Opção (4) Remover Arquivo"
	echo "Opção (5) Sair"
	
	read -p "Digite a opção Desejada: " op
	case $op in
		1) read -p "Digite o nome do arquivo: " nome;;
		2) > teste_de_arquivos/$nome.txt && echo "Arquivo $nome.txt criado com sucesso";;
		3) read -p "digite um numero e o nome do arquivo de deseja inserir: " num insert_name && echo $num >> teste_de_arquivos/$insert_name.txt;;
		4) read -p "Digite o nome do arq que deseja deletar: " del && rm teste_de_arquivos/$del.txt;;
		5) break;;
	esac
	
done	
