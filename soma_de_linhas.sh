#!/bin/bash

total=0
for i in $*; do
	linhas=$(wc -l < $i)
	total=$((total+linhas))
done
echo $total
