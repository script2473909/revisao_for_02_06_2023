#!/bin/bash

dir1=/etc
dir2=/tmp

for i in $dir1/*; do
	if [ -f $i ]; then
		echo o arquivo $i é um arquivo
	elif [ -d $i ]; then
		echo o arquivo $i é um diretório
	elif [ -L $i ]; then
		echo o arquivo $i é um link simbolico
	elif [ -x $i ]; then
		echo o arquivo $i é um executável
	fi
done >> verificacao_etc.txt

for i in $dir2/*; do
	if [ -f $i ]; then
		echo "o arquivo $i é um arquivo"
	elif [ -d $i ]; then
		echo o arquivo $i é um diretório
	elif [ -L $i ]; then
		echo o arquivo $i é um link simbolico
	elif [ -x $i ]; then
		echo o arquivo $i é um executável
	fi
done >> verificacao_tmp.txt
